import { switchMap } from "rxjs/operators";
import { of } from "rxjs";
import { BinaryId, DocumentInterface, QueryTypeBody } from "@litbase/core";
import { Collections } from "@my-app/common/models/collections";
import { useObservable } from "@litbase/use-observable";
import { client } from "@litbase/client";

export function useNewOrExistingDocument<T extends DocumentInterface>(
  idOrNew: string,
  collectionsType: Collections,
  createEmpty: () => T,
  query: QueryTypeBody
) {
  return useObservable<T | null, [string]>(
    (inputs$) =>
      inputs$.pipe(
        switchMap(([idOrNew]) => {
          return idOrNew === "new"
            ? of(createEmpty())
            : client.litql.queryTypeOne<T>(collectionsType, {
                $match: { _id: BinaryId.fromUrlSafeString(idOrNew) },

                ...query,
              });
        })
      ),
    null,
    [idOrNew]
  );
}
