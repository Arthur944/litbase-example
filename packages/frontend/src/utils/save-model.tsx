import { Collections } from "@my-app/common/models/collections";
import { DocumentInterface } from "@litbase/core";
import { showErrorToast, showSuccessToast } from "../styles/toaster-service";
import { createSlug } from "./create-slug";
import { get } from "lodash-es";
import { client } from "@litbase/client";

export async function saveModel<T extends DocumentInterface>({
  values,
  slugField,
  collection,
}: {
  values: T;
  slugField?: string;
  collection: Collections;
}) {
  try {
    const { _id, ...settableValues } = values;

    const slug = slugField ? createSlug(get(settableValues, slugField)) : undefined;
    if (slug) {
      const existingItem = await client.litql.queryTypeOneOnce<T>(collection, {
        $match: { slug },
      });
      if (existingItem && !existingItem._id.equals(_id)) {
        throw new Error("Már létezik elem ilyen címmel!");
      }
    }

    await client.litql.queryTypeOnce(collection, {
      $match: {
        _id,
      },
      $upsert: {
        $setOnInsert: {
          _id,
        },
        $set: {
          ...settableValues,
          ...(slug ? { slug } : {}),
        },
      },
    });
    showSuccessToast(<>Saved successfully</>, null);
  } catch (e) {
    showErrorToast(<>An error occurred</>, <>{e.message}</>);
    throw new Error(e);
  }
}
