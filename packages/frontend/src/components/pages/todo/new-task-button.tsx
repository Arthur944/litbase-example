import { useState } from "react";
import { AddButton, Button, FieldLabel, spacing4, spacing6 } from "@litbase/alexandria";
import { Field, Form, Formik } from "formik";
import styled from "styled-components";
import { client } from "@litbase/client";
import { Collections } from "@my-app/common/models/collections";
import { Task } from "@my-app/common/models/task";
import { createBinaryId } from "@litbase/core";
import { useCurrentUser } from "@litbase/react";

export function NewTaskButton() {
  const [editing, setEditing] = useState(false);
  const user = useCurrentUser();
  if (!editing) {
    return <AddButton onClick={() => setEditing(true)}>Add task</AddButton>;
  }
  return (
    <Formik<Omit<Task, "image"> & { image?: File }>
      initialValues={{
        _id: createBinaryId(),
        name: "",
        isDone: false,
        userId: user._id,
      }}
      onSubmit={async ({ image, ...values }) => {
        if (image) {
          const uploadResults = await client.uploadFile(image).toPromise();
          if (uploadResults && uploadResults.contentUrl) {
            (values as Task).image = uploadResults.contentUrl;
          }
        }
        await client.litql.query(Collections.TASKS, {
          $match: { _id: values._id },
          $upsert: {
            $set: values,
          },
        });
        setEditing(false);
      }}
    >
      {({ setFieldValue }) => (
        <TaskForm>
          <Row>
            <h2>Add new task</h2>
            <CancelButton>Cancel</CancelButton>
          </Row>
          <FieldLabel>Name</FieldLabel>
          <Field name={"name"} />
          <FieldLabel>Image</FieldLabel>
          <input name={"image"} type={"file"} onChange={(e) => setFieldValue("image", e.currentTarget.files[0])} />
          <SaveButtonContainer>
            <Button type={"submit"}>OK</Button>
          </SaveButtonContainer>
        </TaskForm>
      )}
    </Formik>
  );
}

function getUploadedFileUrl(fileName: string) {
  return process.env.SERVER_BASE_URL + fileName;
}

const SaveButtonContainer = styled.div`
  margin-top: ${spacing4};
`;

const CancelButton = styled(Button).attrs(() => ({ $kind: "link" }))`
  margin-left: ${spacing6};
  height: fit-content;
  color: white;
  text-decoration: underline;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
`;

const TaskForm = styled(Form)`
  margin: ${spacing4};
`;
