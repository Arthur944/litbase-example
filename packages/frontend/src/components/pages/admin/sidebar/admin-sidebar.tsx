import { BehaviorSubject } from "rxjs";
import { useEffect } from "react";
import { useAdminConfig } from "../common/admin-context";
import { AccordionWrapper } from "@litbase/alexandria/components/sidebar/accordion";
import { Accordion } from "../../../basic/accordion";
import { AdminSidebarBody } from "./admin-sidebar-body";
import { SidebarItem } from "./sidebar-item";

const sidebarSubject = new BehaviorSubject<boolean>(false);

export function AdminSidebar() {
  const { entities, prefix } = useAdminConfig();
  useEffect(() => {
    document.addEventListener("click", (e) => {
      if (!(e.target as Element).closest(".sidebar") || (e.target as Element).closest("a")) {
        sidebarSubject.next(false);
      }
    });
  }, []);
  return (
    <AdminSidebarBody>
      <AccordionWrapper>
        <Accordion>
          {entities.map((elem, index) => (
            <SidebarItem
              to={prefix + "/admin/" + elem.name}
              name={elem.humanReadableName}
              icon={elem.icon}
              key={index}
            />
          ))}
        </Accordion>
      </AccordionWrapper>
    </AdminSidebarBody>
  );
}
