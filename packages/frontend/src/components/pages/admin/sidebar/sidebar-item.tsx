import { ChevronLeft, Circle } from "@styled-icons/boxicons-regular";
import { ReactNode, useEffect, useRef } from "react";
import { StyledIcon, StyledIconBase } from "@styled-icons/styled-icon";
import { match, matchPath, NavLink, NavLinkProps, useLocation } from "react-router-dom";
import { transitions } from "polished";
import styled from "styled-components";
import { roundedLg } from "@litbase/alexandria";
import { Collapsible, CollapsibleContent, useCollapsible } from "../../../basic/collapsible";

interface SidebarItemProps {
  icon?: StyledIcon;
  name?: string;
  to?: string;
  children?: ReactNode;
  bubbleNumber?: number;
}

export function SidebarItem({ icon: Icon = Circle, name, to, children, bubbleNumber }: SidebarItemProps) {
  if (to) {
    return (
      <SidebarItemContent as={SidebarLink} to={to} exact>
        <Icon />{" "}
        <span>
          {name} {bubbleNumber && bubbleNumber > 0 && <Bubble>{bubbleNumber}</Bubble>}
        </span>
      </SidebarItemContent>
    );
  } else if (children) {
    return (
      <Collapsible>
        <SidebarItemLabel>
          <Icon />{" "}
          <span>
            {name} {bubbleNumber && bubbleNumber > 0 && <Bubble>{bubbleNumber}</Bubble>}
          </span>{" "}
          <ChevronLeft />
        </SidebarItemLabel>
        <CollapsibleContent>
          <SidebarCollapsibleContent>{children}</SidebarCollapsibleContent>
        </CollapsibleContent>
      </Collapsible>
    );
  }

  return null;
}

function SidebarItemLabel({ children }: { children: ReactNode }) {
  const { isOpen, setIsOpen } = useCollapsible();

  return (
    <SidebarItemContent className={isOpen ? "collapsible" : undefined} onClick={() => setIsOpen(!isOpen)}>
      {children}
    </SidebarItemContent>
  );
}

export const Bubble = styled.span<{ $slim?: boolean }>`
  font-size: ${({ theme }) => theme.textXs};
  margin-left: ${({ theme }) => theme.spacing2};
  padding: 0 ${({ theme, $slim }) => ($slim ? theme.spacing2 : theme.spacing3)};
  background-color: ${({ theme }) => theme.primary300};
  color: ${({ theme }) => theme.primary800};
  border-radius: ${roundedLg};
  height: fit-content;
`;

const SidebarItemContent = styled.div`
  display: flex;
  align-items: center;
  margin: 0 ${({ theme }) => theme.spacing2};
  padding: ${({ theme }) => theme.spacing2} ${({ theme }) => theme.spacing3};
  ${({ theme }) => transitions(["background-color", "color"], theme.defaultTransitionTiming)};
  color: ${({ theme }) => theme.gray400};
  outline: none;
  text-decoration: none !important;
  cursor: pointer;
  border-radius: ${({ theme }) => theme.roundedMd};
  margin-top: ${({ theme }) => theme.spacing2};

  &:active,
  &:hover,
  &:focus,
  &.active {
    color: white;
  }

  &.active:not(.collapsible) {
    background-color: ${({ theme }) => theme.gray900};
  }

  &.collapsible > ${StyledIconBase}:last-child:not(:first-child) {
    transform: rotate(-90deg);
  }

  > span {
    flex-grow: 1;
    display: flex;
    flex-wrap: nowrap;
    align-items: center;
  }

  > ${StyledIconBase} {
    font-size: ${({ theme }) => theme.text2xl};
  }

  > ${StyledIconBase}:first-child {
    margin-right: ${({ theme }) => theme.spacing2};
  }

  > ${StyledIconBase}:last-child:not(:first-child) {
    transform: rotate(0deg);
    ${({ theme }) => transitions(["transform"], theme.defaultTransitionTiming)};
  }
`;

const SidebarCollapsibleContent = styled.div`
  overflow: hidden; // Prevent margin collapse
  padding: 0 0 0 ${({ theme }) => theme.spacing6};

  ${SidebarItemContent} {
    > ${StyledIconBase} {
      font-size: ${({ theme }) => theme.textXl};
    }
  }
`;

function SidebarLink(props: NavLinkProps) {
  const isInitialCall = useRef(true);

  const { setIsOpen } = useCollapsible();
  const currentLocation = useLocation();

  useEffect(() => {
    const match = matchPath(currentLocation.pathname, {
      path: props.to as string,
      exact: props.exact,
      strict: props.strict,
    });

    if (match) setIsOpen(true, !isInitialCall.current);

    isInitialCall.current = false;
  }, [currentLocation, props.to, props.exact, props.strict]);

  // @ts-ignore
  return <NavLink isActive={isSidebarLinkActive} {...props} />;
}

function isSidebarLinkActive(match: match<{ id?: string }> | null, location: Location) {
  // Only match if the match is either exact (/new routers will be exact) or the current url does not end in
  // "/new". This is so list links will not match "/new" urls.
  return !!match && (match.isExact || !location.pathname.endsWith("/new"));
}
