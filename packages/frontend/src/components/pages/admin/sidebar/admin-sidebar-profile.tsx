import { StyledIconBase } from "@styled-icons/styled-icon";
import { UserCircle } from "@styled-icons/boxicons-solid";
import { useState } from "react";
import styled from "styled-components";
import { useCurrentUser } from "@litbase/react";
import { Button, LoadingIcon } from "@litbase/alexandria";
import { client } from "@litbase/client";
import { User } from "@my-app/common/models/user";

export function SidebarProfile() {
  const user = useCurrentUser<User>();
  const [isLoading, setIsLoading] = useState(false);

  async function onLogoutClick() {
    setIsLoading(true);
    await client.logout();
  }

  return (
    <>
      <StyledSidebarProfile>
        <SidebarProfileImage>
          <UserCircle />
        </SidebarProfileImage>
        <SidebarProfileDetails>
          <SidebarProfileName>{user.name}</SidebarProfileName>
          <Row>
            <LogoutButton disabled={isLoading} onClick={onLogoutClick}>
              {isLoading && <LoadingIcon />}
              Log out
            </LogoutButton>
          </Row>
        </SidebarProfileDetails>
      </StyledSidebarProfile>
    </>
  );
}

const Row = styled.div`
  display: flex;
`;

const StyledSidebarProfile = styled.div`
  display: flex;
  border-radius: 10px;
  align-items: center;
  background-color: ${(props) => props.theme.gray700};
  padding: ${({ theme }) => theme.spacing3};
`;

const SidebarProfileImage = styled.div`
  margin-right: ${({ theme }) => theme.spacing2};

  ${StyledIconBase} {
    font-size: ${({ theme }) => theme.text5xl};
  }
`;

const SidebarProfileDetails = styled.div`
  flex-grow: 1;
`;

const SidebarProfileName = styled.div`
  margin-bottom: ${({ theme }) => theme.spacing2};
`;

const LogoutButton = styled(Button).attrs({ $kind: "none", $size: "sm" })`
  background-color: ${({ theme }) => theme.gray600};
  border-color: transparent;
  color: ${({ theme }) => theme.gray200};

  &:hover {
    background-color: ${({ theme }) => theme.gray500};
    color: ${({ theme }) => theme.white};
  }
`;
