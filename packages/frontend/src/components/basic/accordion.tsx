import { createContext, ReactNode, useState } from "react";
import { identity } from "lodash-es";
import styled from "styled-components";

export const AccordionContext = createContext<AccordionContextType>({
  exists: false,
  active: null,
  setActive: identity,
  animate: false,
});

export interface AccordionContextType {
  exists: boolean;
  active: number | string | null;
  setActive(value: number | string | null, animate: boolean): void;
  animate: boolean;
}

export interface AccordionProps {
  children?: ReactNode;
}

export function Accordion({ children }: AccordionProps) {
  const [contextValue, setContextValue] = useState<AccordionContextType>(() => ({
    exists: true,
    active: "",
    setActive(value, animate) {
      // Promise.resolve() prevents "cannot update component from inside other components" error, by deferring the
      // state change
      Promise.resolve().then(() =>
        setContextValue({
          ...contextValue,
          active: value,
          animate,
        })
      );
    },
    animate: false,
  }));

  return <AccordionContext.Provider value={contextValue}>{children}</AccordionContext.Provider>;
}

export const AccordionWrapper = styled.div`
  flex: 1 1 0;
  overflow: auto;
`;
