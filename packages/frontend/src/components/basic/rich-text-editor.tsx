import { CKEditor, Editor, EventInfo, FileLoaderInterface } from "@ckeditor/ckeditor5-react";
import DecoupledEditor from "@ckeditor/ckeditor5-build-decoupled-document";
import { CkeditorUploadAdapter } from "./rich-text-editor/ckeditor-upload-adapter";
import { useFastField } from "@litbase/alexandria";

export function RichTextEditor({ value, onChange }: { value: string; onChange: (value: string) => void }) {
  return (
    <CKEditor
      editor={DecoupledEditor}
      data={value}
      onChange={(event: EventInfo, editor: Editor) => {
        onChange(editor.getData());
      }}
      onReady={(editor) => {
        editor.ui
          .getEditableElement()
          .parentElement?.insertBefore(editor.ui.view.toolbar.element, editor.ui.getEditableElement());

        editor.plugins.get("FileRepository").createUploadAdapter = (loader: FileLoaderInterface) => {
          return new CkeditorUploadAdapter(loader);
        };
      }}
    />
  );
}

export function RichTextEditorField({ name }: { name: string }) {
  const [{ value }, , { setValue }] = useFastField(name);
  return <RichTextEditor value={value} onChange={setValue} />;
}
