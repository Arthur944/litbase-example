import { Subject, takeUntil } from "rxjs";
import { client } from "@litbase/client";
import { map } from "rxjs/operators";
import { FileLoaderInterface } from "@ckeditor/ckeditor5-react";

export class CkeditorUploadAdapter {
  loader: FileLoaderInterface;
  cancel$: Subject<boolean>;

  constructor(loader: FileLoaderInterface) {
    this.loader = loader;
    this.cancel$ = new Subject();
  }

  // Starts the upload process.
  async upload() {
    const file = await this.loader.file;
    if (file) {
      return await client
        .uploadFile(file)
        .pipe(
          takeUntil(this.cancel$),
          map((elem) => {
            this.loader.uploadTotal = 100;
            this.loader.uploaded = elem.progress;
            return { default: elem.contentUrl };
          })
        )
        .toPromise();
    }
  }

  // Aborts the upload process.
  abort() {
    this.cancel$.next(true);
  }
}
