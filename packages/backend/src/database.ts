import { litql, RpcRequest } from "@litbase/core";
import { Collections } from "@my-app/common/models/collections";
import { User } from "@my-app/common/models/user";

export function registerJoins() {}

/** Add a hook that only allows changing users if the initiator is an admin.  */
litql.on<"beforeUpsert">("beforeUpsert", async (e) => {
  if ([Collections.USERS as string].includes(e.resolution.collection.name)) {
    await onlyAllowIfAdmin(e.resolution.context.request as RpcRequest);
  }
});

/** This hook makes every registered user become an admin. */
litql.on<"afterInsert">("afterInsert", (e) => {
  if (e.resolution.collection.name === Collections.USERS) {
    litql.query(Collections.USERS, { $match: { _id: e.items[0]._id }, $update: { $set: { isAdmin: true } } });
  }
});

async function onlyAllowIfAdmin(request?: RpcRequest) {
  const userId = request?.client?.auth.user._id;
  if (!userId) throw new Error("User isn't admin!");
  const user = await litql.query<User>(Collections.USERS, { $match: { _id: userId }, $live: false, isAdmin: 1 });
  if (!user?.isAdmin) {
    throw new Error("User isn't admin!");
  }
}
