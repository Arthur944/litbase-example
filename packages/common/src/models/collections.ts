// Names of all available collections the application uses.
export enum Collections {
  USERS = "users",
  TASKS = "tasks",
}
